package lab06;

public class MainToTest {
	
	public static void main(String[] args) {
		RpsGame test = new RpsGame();
		
		System.out.println(test.playRound("scissors"));
		System.out.println(test.getWins());
		System.out.println(test.getLosses());
		System.out.println(test.getTies());
	}
	
	
}
