/**
 * @author Jackson Kwan
 */
package lab06;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.GREEN);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		//overall container
		VBox container = new VBox();
		
		//buttons
		HBox buttons = new HBox();
		Button rockb = new Button("rock");
		Button paperb = new Button("paper");
		Button scissorsb = new Button("scissors");
		
		//textFields
		HBox textFields = new HBox();
		TextField messages = new TextField("Welcome!");
		TextField winsnum = new TextField("wins: 0");
		TextField lossesnum = new TextField("losses: 0");
		TextField tiesnum = new TextField("ties: 0");
		messages.setPrefWidth(200);
		winsnum.setPrefWidth(200);
		lossesnum.setPrefWidth(200);
		tiesnum.setPrefWidth(200);
		
		//listeners
		RpsGame newgame = new RpsGame();
		RpsChoice b1 = new RpsChoice(messages, winsnum, lossesnum, tiesnum, "rock", newgame);
		RpsChoice b2 = new RpsChoice(messages, winsnum, lossesnum, tiesnum, "paper", newgame);
		RpsChoice b3 = new RpsChoice(messages, winsnum, lossesnum, tiesnum, "scissors", newgame);
		
		rockb.setOnAction(b1);
		paperb.setOnAction(b2);
		scissorsb.setOnAction(b3);
		//appending
		root.getChildren().addAll(container);
		container.getChildren().addAll(buttons);
		container.getChildren().addAll(textFields);
		textFields.getChildren().addAll(messages, winsnum, lossesnum, tiesnum);
		buttons.getChildren().addAll(rockb, paperb, scissorsb);
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
