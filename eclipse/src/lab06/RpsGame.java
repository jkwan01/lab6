/**
 * @author Jackson Kwan
 */
package lab06;
import java.util.*;

public class RpsGame {
	private int wins;
	private int losses;
	private int ties;
	private Random rand = new Random();
	
	public int getWins() {
		return wins;
	}
	public int getLosses() {
		return losses;
	}
	public int getTies() {
		return ties;
	}
	
	public String playRound(String choice) {
		String comp = "";
		int num = rand.nextInt(3);
		
		switch (num) {
		case 0: {
			comp = "rock";
			break;
		}
		case 1: {
			comp = "paper";
			break;
		}
		case 2: {
			comp = "scissors";
			break;
		}
		}
		
		if ((choice.toLowerCase().equals("rock") && comp.equals("paper")) || (choice.toLowerCase().equals("paper") && comp.equals("scissors")) || (choice.toLowerCase().equals("scissors") && comp.equals("rock"))) {
			losses++;
			return "Computer wins by playing " + comp;
		}
		if ((choice.toLowerCase().equals("rock") && comp.equals("scissors")) || (choice.toLowerCase().equals("paper") && comp.equals("rock")) || (choice.toLowerCase().equals("scissors") && comp.equals("paper"))) {
			wins++;
			return "You win by playing " + choice.toLowerCase();
		}
		else {
			ties++;
			return "Tie";
		}
	}
}
