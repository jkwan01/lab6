/**
 * @author Jackson Kwan
 */
package lab06;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField msg;
	private TextField win;
	private TextField loss;
	private TextField tie;
	private String playerchoice;
	private RpsGame game;
	
	public RpsChoice(TextField msg, TextField win, TextField loss, TextField tie, String playerchoice, RpsGame game) {
		this.msg = msg;
		this.win = win;
		this.loss = loss;
		this.tie = tie;
		this.playerchoice = playerchoice;
		this.game = game;
	}
	
	@Override
	public void handle(ActionEvent e) {
		String message = game.playRound(playerchoice);
		msg.setText(message);
		win.setText("wins: " + game.getWins());
		loss.setText("losses: " + game.getLosses());
		tie.setText("ties: " + game.getTies());
	}
}
